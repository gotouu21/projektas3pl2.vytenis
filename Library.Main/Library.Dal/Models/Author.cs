﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Models
{
    public class Author
    {
        public int? AuthorId { get; set; }
        
        public string FirstName { get; set; }

        public string LastName { get; set; }

        private Author()
        {

        }

        public Author(string firstName, string lastName, int? authorId = null)
        {
            AuthorId = authorId;
            FirstName = firstName;
            LastName = lastName;
        }
    }
}
