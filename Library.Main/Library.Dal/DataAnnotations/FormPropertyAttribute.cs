﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Dal.DataAnnotations
{
    [AttributeUsage(AttributeTargets.Property)]
    public class FormPropertyAttribute : Attribute
    {
        public string ControlName { get; set; }

		public InputType ControlType { get; set; }
    }
}
