﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Dal.DataAnnotations
{
	[AttributeUsage(AttributeTargets.Property)]
	public class InputTypeAttribute : Attribute
	{
		public InputType Type { get; set; }

		public InputTypeAttribute()
		{

		}
	}

	public enum InputType
	{
		TextBox = 1,
		ComboBox = 2,
		ListBox = 3,

	}
}
