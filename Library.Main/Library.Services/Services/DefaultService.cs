﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Reflection;
using Library.Dal.DataAnnotations;

namespace Library.Services
{
    public class DefaultService : DbConnection
    {
        public DefaultService()
        {

        }

        protected void OpenConnection()
        {
            if (libraryConnection.State == System.Data.ConnectionState.Closed)
            {
                libraryConnection.Open();
            }
        }

        protected void CloseConnection()
        {
            if (libraryConnection.State == System.Data.ConnectionState.Open)
            {
                libraryConnection.Close();
            }
        }		
	}
}
