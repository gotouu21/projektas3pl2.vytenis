﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Dal.DataAnnotations;

namespace Library.ViewModels
{
    public class AuthorViewModel
    {
        [FormProperty(ControlName = "txtAuthorId", ControlType = InputType.TextBox)]
        public int? Id { get; set; }

        [FormProperty(ControlName = "txtAuthorFirstname", ControlType = InputType.TextBox)]
        public string FirstName { get; set; }

        [FormProperty(ControlName = "txtAuthorLastname", ControlType = InputType.TextBox)]
        public string LastName { get; set; }

        public string Fullname { get { return $"{FirstName} {LastName}"; } }
    }
}
